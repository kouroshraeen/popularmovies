# Popular Movies

Popular Movies is the first student project for the Udacity course [Developing Android Apps: Android Fundamentals](https://www.udacity.com/course/ud853).

Note: You will need to provide your own themoviedb.org API key for the app to work. Open or create the gradle.properties file located in .gradle directory
on your local machine and add the following line: TheMovieDBAPIKey=YOUR_API_KEY.

You could also open the MovieListFragment.java file and change the line:

    Call<MovieResult> call = movieDatabaseApi.getMovies(BuildConfig.THE_MOVIE_DB_API_KEY, mSortOrder);

to:

    Call<MovieResult> call = movieDatabaseApi.getMovies(YOUR_API_KEY, mSortOrder);

where you replace YOUR_API_KEY with your own key.

## Screenshots
![screen](./doc/screenshots/movies-grid.png)
![screen](./doc/screenshots/movie-detail.png)
![screen](./doc/screenshots/favorited-movie-toast.png)
![screen](./doc/screenshots/reviews-trailers.png)
![screen](./doc/screenshots/sort-options.png)

## Libraries

* [ButterKnife](https://github.com/JakeWharton/butterknife)
* [Retrofit](https://github.com/square/retrofit)
* [Picasso](https://github.com/square/picasso)
