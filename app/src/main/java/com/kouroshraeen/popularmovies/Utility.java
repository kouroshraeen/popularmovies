package com.kouroshraeen.popularmovies;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AlertDialog;

import com.kouroshraeen.popularmovies.data.MovieDbHelper;
import com.kouroshraeen.popularmovies.data.MovieProvider;
import com.kouroshraeen.popularmovies.models.Movie;

import java.util.ArrayList;

public class Utility {

    private static final String LOG_TAG = Utility.class.getSimpleName();

    public static Boolean isNetworkAvailable(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static void showNoNetworkDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        String title = activity.getString(R.string.no_internet_text);
        String message = "Please make sure that Cellular data is on or you are connected to Wi-Fi.";
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void updateFavoriteMovie(Activity activity, Movie movie) {
        if (isFavorite(activity, movie.getId())) {
            // delete from favorite list
            String selection = MovieDbHelper.MOVIE_ID + "=" + movie.getId();
            activity.getContentResolver().delete(MovieProvider.CONTENT_URI, selection, null);
        }
        else {
            ContentValues values = new ContentValues();
            values.put(MovieDbHelper.MOVIE_ID, movie.getId());
            values.put(MovieDbHelper.MOVIE_TITLE, movie.getOriginalTitle());
            values.put(MovieDbHelper.MOVIE_OVERVIEW, movie.getOverview());
            values.put(MovieDbHelper.MOVIE_RELEASE_DATE, movie.getReleaseDate());
            values.put(MovieDbHelper.MOVIE_POSTER_IMAGE_URL, movie.getPosterImageURL());
            values.put(MovieDbHelper.MOVIE_VOTER_AVERAGE, movie.getVoteAverage());

            activity.getContentResolver().insert(MovieProvider.CONTENT_URI, values);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static ArrayList<Movie> getFavoriteMovies(Activity activity) {
        Cursor cursor = activity.getContentResolver().query(MovieProvider.CONTENT_URI, MovieDbHelper.ALL_COLUMNS, null, null, null, null);
        ArrayList<Movie> movies = new ArrayList<>();
        if (cursor != null) {
            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {
                Movie movie = new Movie();
                movie.setId(cursor.getString(cursor.getColumnIndex(MovieDbHelper.MOVIE_ID)));
                movie.setOriginalTitle(cursor.getString(cursor.getColumnIndex(MovieDbHelper.MOVIE_TITLE)));
                movie.setOverview(cursor.getString(cursor.getColumnIndex(MovieDbHelper.MOVIE_OVERVIEW)));
                movie.setReleaseDate(cursor.getString(cursor.getColumnIndex(MovieDbHelper.MOVIE_RELEASE_DATE)));
                movie.setPosterImageURL(cursor.getString(cursor.getColumnIndex(MovieDbHelper.MOVIE_POSTER_IMAGE_URL)));
                movie.setVoteAverage(cursor.getDouble(cursor.getColumnIndex(MovieDbHelper.MOVIE_VOTER_AVERAGE)));
                movies.add(movie);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return movies;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean isFavorite(Activity activity, String id) {
        Boolean favorite = false;
        String selection = MovieDbHelper.MOVIE_ID + "=" + id;
        Cursor cursor = activity.getContentResolver().query(MovieProvider.CONTENT_URI, MovieDbHelper.ALL_COLUMNS, selection, null, null, null);
        if (cursor != null) {
            favorite = (cursor.getCount() > 0);
            cursor.close();
        }
        return (favorite);
    }
}
