package com.kouroshraeen.popularmovies;

import com.kouroshraeen.popularmovies.models.MovieResult;
import com.kouroshraeen.popularmovies.models.ReviewResult;
import com.kouroshraeen.popularmovies.models.TrailerResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Kourosh on 10/20/2015.
 */
public interface MovieDatabaseApi {

    public static final String BASE_URL = "http://api.themoviedb.org/3/";

    @GET("discover/movie")
    Call<MovieResult> getMovies(@Query("api_key") String apiKey, @Query("sort_by") String sortBy);

    @GET("movie/{movie_id}/reviews")
    Call<ReviewResult> getReviews(@Path("movie_id") String id, @Query("api_key") String apiKey);

    @GET("movie/{movie_id}/videos")
    Call<TrailerResult> getTrailers(@Path("movie_id") String id, @Query("api_key") String apiKey);
}
