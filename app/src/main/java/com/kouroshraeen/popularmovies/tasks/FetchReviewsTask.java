package com.kouroshraeen.popularmovies.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.kouroshraeen.popularmovies.BuildConfig;
import com.kouroshraeen.popularmovies.models.Review;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kourosh on 9/27/2015.
 */
public class FetchReviewsTask extends AsyncTask<Review, Void, List<Review>> {

    private final String LOG_TAG = FetchReviewsTask.class.getSimpleName();
    private String mMovieId;
    OnReviewsFetchedListener mListener;

    public FetchReviewsTask(String movieId, OnReviewsFetchedListener listener) {
        mMovieId = movieId;
        mListener = listener;
    }

    public interface OnReviewsFetchedListener {
        void onReviewsFetched(List<Review> reviews);
    }

    private List<Review> getReviewsDataFromJson(String reviewsJsonStr)
            throws JSONException {

        JSONObject reviewsJson = new JSONObject(reviewsJsonStr);
        JSONArray reviewsArray = reviewsJson.getJSONArray("results");

        ArrayList<Review> reviews = new ArrayList<Review>();

        for (int i = 0; i < reviewsArray.length(); i++) {
            JSONObject currentReviewJson = reviewsArray.getJSONObject(i);
            Review review = new Review();

            review.setId(currentReviewJson.getString("id"));
            review.setAuthor(currentReviewJson.getString("author"));
            review.setContent(currentReviewJson.getString("content"));
            reviews.add(review);
        }

        return reviews;
    }

    @Override
    protected List<Review> doInBackground(Review... params) {

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String reviewsJsonStr = null;

        try {
            // Construct the URL for the themoviedb.org query
            URL url = new URL("http://api.themoviedb.org/3/movie/" + mMovieId + "/reviews?api_key=" + BuildConfig.THE_MOVIE_DB_API_KEY);

            // Create the request to themoviedb.org, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            reviewsJsonStr = buffer.toString();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try {
            return getReviewsDataFromJson(reviewsJsonStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Review> reviews) {
        super.onPostExecute(reviews);
        mListener.onReviewsFetched(reviews);
    }
}
