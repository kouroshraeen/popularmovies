package com.kouroshraeen.popularmovies.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.kouroshraeen.popularmovies.BuildConfig;
import com.kouroshraeen.popularmovies.models.Trailer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kourosh on 9/27/2015.
 */
public class FetchTrailersTask extends AsyncTask<Trailer, Void, List<Trailer>> {

    private final String LOG_TAG = FetchTrailersTask.class.getSimpleName();
    private String mMovieId;
    OnTrailersFetchedListener mListener;

    public FetchTrailersTask(String movieId, OnTrailersFetchedListener listener) {
        mMovieId = movieId;
        mListener = listener;
    }

    public interface OnTrailersFetchedListener {
        void onTrailersFetched(List<Trailer> trailers);
    }

    private List<Trailer> getTrailersDataFromJson(String trailersJsonStr)
            throws JSONException {

        JSONObject trailersJson = new JSONObject(trailersJsonStr);
        JSONArray trailersArray = trailersJson.getJSONArray("results");

        ArrayList<Trailer> trailers = new ArrayList<Trailer>();

        for (int i = 0; i < trailersArray.length(); i++) {
            JSONObject currentTrailerJson = trailersArray.getJSONObject(i);
            Trailer trailer = new Trailer();

            trailer.setId(currentTrailerJson.getString("id"));
            trailer.setKey(currentTrailerJson.getString("key"));
            trailer.setName(currentTrailerJson.getString("name"));
            trailers.add(trailer);
        }

        return trailers;
    }

    @Override
    protected List<Trailer> doInBackground(Trailer... params) {

        // These two need to be declared outside the try/catch
        // so that they can be closed in the finally block.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String trailersJsonStr = null;

        try {
            // Construct the URL for the themoviedb.org query
            URL url = new URL("http://api.themoviedb.org/3/movie/" + mMovieId + "/videos?api_key=" + BuildConfig.THE_MOVIE_DB_API_KEY);

            // Create the request to themoviedb.org, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }
            trailersJsonStr = buffer.toString();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }

        try {
            return getTrailersDataFromJson(trailersJsonStr);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(List<Trailer> trailers) {
        super.onPostExecute(trailers);
        mListener.onTrailersFetched(trailers);
    }
}
