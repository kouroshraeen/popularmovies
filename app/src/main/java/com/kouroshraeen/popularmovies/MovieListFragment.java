package com.kouroshraeen.popularmovies;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.kouroshraeen.popularmovies.models.Movie;
import com.kouroshraeen.popularmovies.models.MovieResult;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieListFragment extends Fragment {

    private final String LOG_TAG = MovieListFragment.class.getSimpleName();

    private ArrayList<Movie> mMovies;
    private MovieAdapter mMovieAdapter;
    private GridView mGridView;
    private String mSortOrder = "popularity.desc";

    Callbacks dataPasser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null || !savedInstanceState.containsKey("movies")) {
            mMovies = new ArrayList<>();
        } else {
            mMovies = savedInstanceState.getParcelableArrayList("movies");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            dataPasser = (Callbacks) context;
        }
    }

    public MovieListFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("movies", mMovies);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);

        mMovieAdapter = new MovieAdapter(getActivity(), mMovies);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        // Get a reference to the GridView, and attach this adapter to it.
        mGridView = (GridView) rootView.findViewById(R.id.movies_grid);
        mGridView.setAdapter(mMovieAdapter);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                ((Callbacks) getActivity()).onItemSelected(mMovieAdapter.getItem(position));
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sortPreferenceChanged = pref.getString("sort_order_changed", "");

        if (sortPreferenceChanged.equals("Yes") || mMovies.isEmpty() || pref.getString("movie_deleted", "").equals("Yes")) {
            updateMovies();
            if (sortPreferenceChanged.equals("Yes")) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("sort_order_changed", "No").apply();
                pref.edit().putString("movie_delete", "No").apply();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean update = false;

        switch (item.getItemId()) {
            case R.id.action_sort_popularity:
                if (!mSortOrder.equals("popularity.desc")) {
                    mSortOrder = "popularity.desc";
                    update = true;
                }
                break;
            case R.id.action_sort_rating:
                if (!mSortOrder.equals("vote_count.desc")) {
                    mSortOrder = "vote_count.desc";
                    update = true;
                }
                break;
            case R.id.action_show_favorites:
                if (!mSortOrder.equals("favorites")) {
                    mSortOrder = "favorites";
                    update = true;
                }
                break;
        }
        if (update) {
            pref.edit().putString("sort_order_changed", "Yes").apply();
            updateMovies();
        }
        return true;

    }

    private void updateMovies() {
        if (!mSortOrder.equals("favorites")) {
            if (Utility.isNetworkAvailable(getActivity())) {

                final Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(MovieDatabaseApi.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                MovieDatabaseApi movieDatabaseApi = retrofit.create(MovieDatabaseApi.class);
                Call<MovieResult> call = movieDatabaseApi.getMovies(BuildConfig.THE_MOVIE_DB_API_KEY, mSortOrder);

                call.enqueue(new retrofit2.Callback<MovieResult>() {
                    @Override
                    public void onResponse(Call<MovieResult> call, Response<MovieResult> response) {
                        MovieResult movieResult = response.body();
                        mMovieAdapter.clear();
                        mMovieAdapter.addAll(movieResult.results);
                        mGridView.smoothScrollToPosition(0);

                        dataPasser.onDataPass(response.body().results.get(0));
                    }

                    @Override
                    public void onFailure(Call<MovieResult> call, Throwable t) {
                        Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            } else {
                Utility.showNoNetworkDialog(getActivity());
            }
        }
        else {
            // Add code to get favorite movies from database
            mMovies = Utility.getFavoriteMovies(getActivity());
            if (mMovies.isEmpty()) {
                Toast.makeText(getActivity(), "There are no movies in the favorites list", Toast.LENGTH_LONG).show();
                mMovieAdapter.clear();
            }
            else {
                mMovieAdapter.clear();
                mMovieAdapter.addAll(mMovies);
                mGridView.smoothScrollToPosition(0);
                dataPasser.onDataPass(mMovies.get(0));
            }
        }
    }

    public interface Callbacks {
        void onItemSelected(Movie movie);
        void onDataPass(Movie movie);
    }

}
