package com.kouroshraeen.popularmovies;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.kouroshraeen.popularmovies.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Kourosh on 8/10/2015.
 */
public class MovieAdapter extends ArrayAdapter<Movie>{

    public MovieAdapter(Activity context, ArrayList<Movie> Movie) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, Movie);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String basePosterImageUrl = "http://image.tmdb.org/t/p/w185/";

        // Gets the Movie object from the ArrayAdapter at the appropriate position
        Movie movie = getItem(position);

        ViewHolder holder;

        // Adapters recycle views to AdapterViews.
        // If this is a new View object we're getting, then inflate the layout.
        // If not, this view already has the layout inflated from a previous call to getView,
        // and we modify the View widgets as usual.
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.movie_item, parent, false);
            holder = new ViewHolder();
            holder.moviePoster = (ImageView) convertView.findViewById(R.id.movie_image);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Uri builtUri = Uri.parse(basePosterImageUrl).buildUpon().appendEncodedPath(movie.getPosterImageURL()).build();

        Picasso.with(getContext()).load(builtUri.toString()).into(holder.moviePoster);

        return convertView;
    }

    private static class ViewHolder {
        ImageView moviePoster;
    }
}
