package com.kouroshraeen.popularmovies;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kouroshraeen.popularmovies.models.Movie;
import com.kouroshraeen.popularmovies.models.Review;
import com.kouroshraeen.popularmovies.models.ReviewResult;
import com.kouroshraeen.popularmovies.models.Trailer;
import com.kouroshraeen.popularmovies.models.TrailerResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDetailFragment extends Fragment {

    private final String LOG_TAG = MovieDetailFragment.class.getSimpleName();
    private final String basePosterImageUrl = "http://image.tmdb.org/t/p/w185/";
    private View mRootView;
    private boolean mFavorite;
    private Movie mMovie;
    private ArrayList<Review> mReviews;
    private ArrayList<Trailer> mTrailers;

    @BindView(R.id.movie_title) TextView titleTextView;
    @BindView(R.id.movie_poster)ImageView posterImageView;
    @BindView(R.id.movie_synopsis) TextView synopsisTextView;
    @BindView(R.id.release_date) TextView releaseDateTextView;
    @BindView(R.id.rating_bar) RatingBar ratingBar;
    @BindView(R.id.favorite_button) FloatingActionButton actionButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null || !savedInstanceState.containsKey("movie")) {
            //mMovie = new Movie();
        } else {
            mMovie = savedInstanceState.getParcelable("movie");
        }
    }

    public MovieDetailFragment() {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("movie", mMovie);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        ButterKnife.bind(this, mRootView);

        Toolbar toolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);

        Intent intent = getActivity().getIntent();

        if (intent != null && intent.hasExtra("movie")) {
            mMovie = intent.getParcelableExtra("movie");
        }

        if (getArguments() != null) {
            mMovie = getArguments().getParcelable("movie");
        }

        if (mMovie != null) {
            SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
            final Boolean twoPane = sharedPref.getBoolean("two_pane", false);

            titleTextView.setText(mMovie.getOriginalTitle());

            Uri builtUri = Uri.parse(basePosterImageUrl).buildUpon().appendEncodedPath(mMovie.getPosterImageURL()).build();

            if (twoPane) {
                Picasso.with(getActivity()).load(builtUri.toString()).resize(185 * 2, 278 * 2).into(posterImageView);
                activity.getSupportActionBar().setTitle(mMovie.getOriginalTitle());
            }
            else {
                Picasso.with(getActivity()).load(builtUri.toString()).resize(185 * 3, 278 * 3).into(posterImageView);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            synopsisTextView.setText(mMovie.getOverview());

            releaseDateTextView.setText("Release date: " + mMovie.getReleaseDate().substring(0, 4));

            float rating = (float) ((mMovie.getVoteAverage() * 5) / 10);
            ratingBar.setStepSize((float) 0.1);

            ratingBar.setRating(rating);

            mFavorite = Utility.isFavorite(getActivity(), mMovie.getId());

            //final FloatingActionButton actionButton = (FloatingActionButton) rootView.findViewById(R.id.favorite_button);
            if (mFavorite) {
                actionButton.setImageResource(R.drawable.ic_action_check);
            }
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFavorite) {
                        actionButton.setImageResource(R.drawable.ic_action_add);
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("movie_deleted", "Yes").commit();
                        mFavorite = false;
                        Toast.makeText(getActivity(), "Movie removed from the favorite list", Toast.LENGTH_LONG).show();
                    } else {
                        actionButton.setImageResource(R.drawable.ic_action_check);
                        mFavorite = true;
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("movie_deleted", "No").commit();
                        Toast.makeText(getActivity(), "Movie added to the favorite list", Toast.LENGTH_LONG).show();
                    }

                    Utility.updateFavoriteMovie(getActivity(), mMovie);


                }
            });

            fetchAndShowReviews();
            fetchAndShowTrailers();

        }
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void fetchAndShowReviews() {
        if (Utility.isNetworkAvailable(getActivity())) {
            mReviews = new ArrayList<>();

            MovieDatabaseApi movieDatabaseApi = getApiService();

            Call<ReviewResult> callReviews = movieDatabaseApi.getReviews(mMovie.getId(), BuildConfig.THE_MOVIE_DB_API_KEY);
            callReviews.enqueue(new retrofit2.Callback<ReviewResult>() {
                @Override
                public void onResponse(Call<ReviewResult> call, Response<ReviewResult> response) {
                    mReviews = response.body().results;

                    ViewGroup reviewContainer = (ViewGroup) mRootView.findViewById(R.id.reviews_container);

                    if (mReviews.isEmpty()) {
                        View view = getActivity().getLayoutInflater().inflate(R.layout.review_item, reviewContainer, false);
                        TextView authorTextView = (TextView) view.findViewById(R.id.review_author);
                        authorTextView.setVisibility(View.GONE);
                        TextView contentTextView = (TextView) view.findViewById(R.id.review_content);
                        contentTextView.setText(R.string.no_reviews_text);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        params.setMargins(48, 16, 0, 0);
                        contentTextView.setLayoutParams(params);
                        reviewContainer.addView(view);
                    }
                    else {
                        for (Review review : mReviews) {

                            View view = getActivity().getLayoutInflater().inflate(R.layout.review_item, reviewContainer, false);

                            TextView authorTextView = (TextView) view.findViewById(R.id.review_author);
                            authorTextView.setText(review.getAuthor());
                            TextView contentTextView = (TextView) view.findViewById(R.id.review_content);
                            contentTextView.setText(review.getContent());
                            reviewContainer.addView(view);
                        }
                    }
                    reviewContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<ReviewResult> call, Throwable t) {

                }
            });
        } else {
            Utility.showNoNetworkDialog(getActivity());
        }
    }

    private void fetchAndShowTrailers() {
        if (Utility.isNetworkAvailable(getActivity())) {
            mReviews = new ArrayList<>();

            MovieDatabaseApi movieDatabaseApi = getApiService();

            Call<TrailerResult> callReviews = movieDatabaseApi.getTrailers(mMovie.getId(), BuildConfig.THE_MOVIE_DB_API_KEY);
            callReviews.enqueue(new retrofit2.Callback<TrailerResult>() {
                @Override
                public void onResponse(Call<TrailerResult> call, Response<TrailerResult> response) {
                    mTrailers = response.body().results;

                    ViewGroup trailerContainer = (ViewGroup) getActivity().findViewById(R.id.trailers_container);

                    for (final Trailer trailer : mTrailers) {

                        View view = getActivity().getLayoutInflater().inflate(R.layout.trailer_item, trailerContainer, false);

                        TextView authorTextView = (TextView) view.findViewById(R.id.trailer_name);
                        authorTextView.setText(trailer.getName());

                        ImageView trailerImageView = (ImageView) view.findViewById(R.id.trailer_image_view);
                        trailerImageView.setColorFilter(getResources().getColor(R.color.accent));
                        trailerImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + trailer.getKey()));
                                startActivity(intent);
                            }
                        });

                        trailerContainer.addView(view);
                    }
                    trailerContainer.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<TrailerResult> call, Throwable t) {

                }
            });
        } else {
            Utility.showNoNetworkDialog(getActivity());
        }
    }

    private MovieDatabaseApi getApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MovieDatabaseApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(MovieDatabaseApi.class);
    }

}
