package com.kouroshraeen.popularmovies.models;

import java.util.ArrayList;

/**
 * Created by Kourosh on 10/22/2015.
 */
public class ReviewResult {
    public int id;
    public int page;
    public ArrayList<Review> results;
    public int total_pages;
    public int total_results;
}
