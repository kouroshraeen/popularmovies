package com.kouroshraeen.popularmovies.models;

import java.util.ArrayList;

/**
 * Created by Kourosh on 10/22/2015.
 */
public class TrailerResult {
    public int id;
    public ArrayList<Trailer> results;
}
