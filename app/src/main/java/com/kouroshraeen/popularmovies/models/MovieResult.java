package com.kouroshraeen.popularmovies.models;

import java.util.ArrayList;

/**
 * Created by Kourosh on 10/20/2015.
 */
public class MovieResult {
    public int page;
    public ArrayList<Movie> results;
    public int total_pages;
    public int total_results;
}
