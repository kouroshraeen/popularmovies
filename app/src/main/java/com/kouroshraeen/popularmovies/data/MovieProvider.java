package com.kouroshraeen.popularmovies.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

/**
 * Created by Kourosh on 9/27/2015.
 */
public class MovieProvider extends ContentProvider {

    private static final String CONTENT_AUTHORITY  = "com.kouroshraeen.popularmovies.movieprovider";
    private static final String BASE_PATH = "favorite_movie";
    public static final Uri CONTENT_URI =
            Uri.parse("content://" + CONTENT_AUTHORITY + "/" + BASE_PATH );

    // Constant to identify the requested operation
    private static final int MOVIES = 1;
    private static final int MOVIE_ID = 2;

    private static final UriMatcher uriMatcher =
            new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(CONTENT_AUTHORITY, BASE_PATH, MOVIES);
        uriMatcher.addURI(CONTENT_AUTHORITY, BASE_PATH +  "/#", MOVIE_ID);
    }

    private SQLiteDatabase database;

    @Override
    public boolean onCreate() {
        MovieDbHelper helper = new MovieDbHelper(getContext());
        database = helper.getWritableDatabase();

        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return database.query(MovieDbHelper.TABLE_NAME, MovieDbHelper.ALL_COLUMNS, selection, null, null, null, null);
    }

    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long id = database.insert(MovieDbHelper.TABLE_NAME, null, values);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        return database.delete(MovieDbHelper.TABLE_NAME, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return database.update(MovieDbHelper.TABLE_NAME, values, selection, selectionArgs);
    }
}
