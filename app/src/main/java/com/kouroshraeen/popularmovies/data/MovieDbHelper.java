package com.kouroshraeen.popularmovies.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Kourosh on 9/27/2015.
 */
public class MovieDbHelper extends SQLiteOpenHelper {

    //Constants for db name and version
    private static final String DATABASE_NAME = "movies.db";
    private static final int DATABASE_VERSION = 1;

    //Constants for identifying table and columns
    public static final String TABLE_NAME = "favorite_movie";
    public static final String ROW_ID = "_id";
    public static final String MOVIE_ID = "movieId";
    public static final String MOVIE_TITLE = "movieTitle";
    public static final String MOVIE_OVERVIEW = "movieOverview";
    public static final String MOVIE_RELEASE_DATE = "movieReleaseDate";
    public static final String MOVIE_POSTER_IMAGE_URL = "moviePosterImageUrl";
    public static final String MOVIE_VOTER_AVERAGE = "movieVoterAverage";

    public static final String[] ALL_COLUMNS =
            {ROW_ID, MOVIE_ID, MOVIE_TITLE, MOVIE_OVERVIEW, MOVIE_RELEASE_DATE, MOVIE_POSTER_IMAGE_URL, MOVIE_VOTER_AVERAGE};

    //SQL to create table
    private static final String SQL_CREATE_FAVORITE_MOVIE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    MOVIE_ID + " TEXT, " +
                    MOVIE_TITLE + " TEXT, " +
                    MOVIE_OVERVIEW + " TEXT, " +
                    MOVIE_RELEASE_DATE + " TEXT, " +
                    MOVIE_POSTER_IMAGE_URL + " TEXT, " +
                    MOVIE_VOTER_AVERAGE + " REAL" +
                    ")";

    public MovieDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FAVORITE_MOVIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
